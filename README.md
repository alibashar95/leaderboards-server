# How to run the backend locally

Note that Docker is required

1. CMD in current dir (the same as where this README file is located)
2. docker build -t leaderboards_backend .\pb
3. docker run -d --name leaderboards_backend -p 8080:8080 -v  {PATH_TO_LOCAL_STORAGE}:/pb/pb_data -v {PATH_TO_MIGRATIONS_FOLDER}:/pb/migrations leaderboards_backend:latest

Replace {PATH_TO_LOCAL_STORAGE} with a folder somewhere on your machine. Make sure that the file is ignored in .gitignore if the folder is the same as the the rest of the files. Do not put it in the pb folder where the Dockerfile is (or add it to .dockerignore if you do so).

Replace {PATH_TO_MIGRATIONS_FOLDER} with the migrations folder in the repository. This is so that the database is correctly migrated, and so that when changes are done locally to the database, migrations are correctly added to the repo so that it can be checked in with version control.

Now the backend should be available at localhost:8080