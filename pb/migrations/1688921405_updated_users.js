migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("_pb_users_auth_")

  collection.listRule = ""
  collection.viewRule = ""
  collection.createRule = null

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "1wwfl12n",
    "name": "isApproved",
    "type": "bool",
    "required": false,
    "unique": false,
    "options": {}
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("_pb_users_auth_")

  collection.listRule = null
  collection.viewRule = null
  collection.createRule = ""

  // remove
  collection.schema.removeField("1wwfl12n")

  return dao.saveCollection(collection)
})
