migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("bmpv4hkmnnq9dvd")

  collection.updateRule = ""

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("bmpv4hkmnnq9dvd")

  collection.updateRule = null

  return dao.saveCollection(collection)
})
