migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("6cm1kdkve4vs17l")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "oonfn6ru",
    "name": "rating_change",
    "type": "number",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("6cm1kdkve4vs17l")

  // remove
  collection.schema.removeField("oonfn6ru")

  return dao.saveCollection(collection)
})
