migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("e9iw5fjo0rt4ggh")

  collection.name = "player_matches"

  // remove
  collection.schema.removeField("k2h8fv2p")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "uiwxab5d",
    "name": "player",
    "type": "relation",
    "required": true,
    "unique": false,
    "options": {
      "collectionId": "bmpv4hkmnnq9dvd",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("e9iw5fjo0rt4ggh")

  collection.name = "user_matches"

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "k2h8fv2p",
    "name": "user",
    "type": "relation",
    "required": true,
    "unique": false,
    "options": {
      "collectionId": "_pb_users_auth_",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  // remove
  collection.schema.removeField("uiwxab5d")

  return dao.saveCollection(collection)
})
