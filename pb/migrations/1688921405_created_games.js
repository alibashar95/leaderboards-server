migrate((db) => {
  const collection = new Collection({
    "id": "78srpjtdesw2wqj",
    "created": "2023-07-09 16:50:05.478Z",
    "updated": "2023-07-09 16:50:05.478Z",
    "name": "games",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "jlpx1hwe",
        "name": "name",
        "type": "text",
        "required": true,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      },
      {
        "system": false,
        "id": "nwhggjb3",
        "name": "image",
        "type": "file",
        "required": true,
        "unique": false,
        "options": {
          "maxSelect": 1,
          "maxSize": 5242880,
          "mimeTypes": [
            "image/svg+xml",
            "image/png",
            "image/jpeg",
            "image/webp"
          ],
          "thumbs": [],
          "protected": false
        }
      },
      {
        "system": false,
        "id": "2bg2tsab",
        "name": "genre",
        "type": "select",
        "required": true,
        "unique": false,
        "options": {
          "maxSelect": 1,
          "values": [
            "Action",
            "Adventure",
            "Role-playing",
            "Strategy",
            "Simulation",
            "Sports",
            "Racing",
            "Puzzle",
            "Platformer",
            "Shooter",
            "Fighting",
            "Stealth",
            "Survival",
            "Sandbox",
            "Open world",
            "Tactical",
            "Massively multiplayer online (MMO)",
            "Turn-based",
            "Card and board games",
            "Educational",
            "IRL Activity"
          ]
        }
      }
    ],
    "indexes": [],
    "listRule": "",
    "viewRule": "",
    "createRule": "@request.auth.isApproved = true",
    "updateRule": null,
    "deleteRule": null,
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("78srpjtdesw2wqj");

  return dao.deleteCollection(collection);
})
