migrate((db) => {
  const collection = new Collection({
    "id": "lw5k1rn1dlo3vtx",
    "created": "2023-07-09 16:50:05.478Z",
    "updated": "2023-07-09 16:50:05.478Z",
    "name": "matches",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "ngtwyixo",
        "name": "game",
        "type": "relation",
        "required": true,
        "unique": false,
        "options": {
          "collectionId": "78srpjtdesw2wqj",
          "cascadeDelete": false,
          "minSelect": null,
          "maxSelect": 1,
          "displayFields": []
        }
      },
      {
        "system": false,
        "id": "fskz8trp",
        "name": "matchType",
        "type": "select",
        "required": false,
        "unique": false,
        "options": {
          "maxSelect": 1,
          "values": [
            "1v1",
            "Teams",
            "FFA"
          ]
        }
      }
    ],
    "indexes": [],
    "listRule": "",
    "viewRule": "",
    "createRule": "@request.auth.isApproved = true",
    "updateRule": "@request.auth.isApproved = true",
    "deleteRule": "@request.auth.isApproved = true",
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("lw5k1rn1dlo3vtx");

  return dao.deleteCollection(collection);
})
