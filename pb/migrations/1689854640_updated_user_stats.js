migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("3qttk2rdzkzv348")

  collection.name = "player_stats"

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("3qttk2rdzkzv348")

  collection.name = "user_stats"

  return dao.saveCollection(collection)
})
