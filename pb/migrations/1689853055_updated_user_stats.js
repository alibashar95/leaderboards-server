migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("3qttk2rdzkzv348")

  // remove
  collection.schema.removeField("asoqzvhk")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "fog1wo4u",
    "name": "player",
    "type": "relation",
    "required": true,
    "unique": false,
    "options": {
      "collectionId": "bmpv4hkmnnq9dvd",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("3qttk2rdzkzv348")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "asoqzvhk",
    "name": "user",
    "type": "relation",
    "required": true,
    "unique": false,
    "options": {
      "collectionId": "_pb_users_auth_",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  // remove
  collection.schema.removeField("fog1wo4u")

  return dao.saveCollection(collection)
})
