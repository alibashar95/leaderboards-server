migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("bmpv4hkmnnq9dvd")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "jddnbyid",
    "name": "name",
    "type": "text",
    "required": true,
    "unique": false,
    "options": {
      "min": 2,
      "max": 50,
      "pattern": ""
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("bmpv4hkmnnq9dvd")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "jddnbyid",
    "name": "name",
    "type": "text",
    "required": true,
    "unique": false,
    "options": {
      "min": 2,
      "max": 20,
      "pattern": ""
    }
  }))

  return dao.saveCollection(collection)
})
