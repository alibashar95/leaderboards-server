migrate((db) => {
  const collection = new Collection({
    "id": "bmpv4hkmnnq9dvd",
    "created": "2023-07-20 11:25:36.935Z",
    "updated": "2023-07-20 11:25:36.935Z",
    "name": "players",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "jddnbyid",
        "name": "name",
        "type": "text",
        "required": true,
        "unique": false,
        "options": {
          "min": 2,
          "max": 20,
          "pattern": ""
        }
      },
      {
        "system": false,
        "id": "trp55foo",
        "name": "avatar",
        "type": "file",
        "required": true,
        "unique": false,
        "options": {
          "maxSelect": 1,
          "maxSize": 5000000,
          "mimeTypes": [
            "image/jpeg",
            "image/svg+xml",
            "image/png",
            "image/heic",
            "image/heif",
            "image/webp"
          ],
          "thumbs": [],
          "protected": false
        }
      }
    ],
    "indexes": [],
    "listRule": "",
    "viewRule": "",
    "createRule": "",
    "updateRule": null,
    "deleteRule": null,
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("bmpv4hkmnnq9dvd");

  return dao.deleteCollection(collection);
})
