migrate((db) => {
  const collection = new Collection({
    "id": "3qttk2rdzkzv348",
    "created": "2023-07-09 16:50:05.478Z",
    "updated": "2023-07-09 16:50:05.478Z",
    "name": "user_stats",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "asoqzvhk",
        "name": "user",
        "type": "relation",
        "required": true,
        "unique": false,
        "options": {
          "collectionId": "_pb_users_auth_",
          "cascadeDelete": false,
          "minSelect": null,
          "maxSelect": 1,
          "displayFields": []
        }
      },
      {
        "system": false,
        "id": "wd5llwol",
        "name": "game",
        "type": "relation",
        "required": true,
        "unique": false,
        "options": {
          "collectionId": "78srpjtdesw2wqj",
          "cascadeDelete": false,
          "minSelect": null,
          "maxSelect": 1,
          "displayFields": []
        }
      },
      {
        "system": false,
        "id": "z46g4whf",
        "name": "wins",
        "type": "number",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null
        }
      },
      {
        "system": false,
        "id": "wsaecixq",
        "name": "losses",
        "type": "number",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null
        }
      },
      {
        "system": false,
        "id": "uz1qzmfk",
        "name": "rating",
        "type": "number",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null
        }
      },
      {
        "system": false,
        "id": "6lm2zn4f",
        "name": "win_streak",
        "type": "number",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null
        }
      }
    ],
    "indexes": [],
    "listRule": "",
    "viewRule": "",
    "createRule": "@request.auth.isApproved = true",
    "updateRule": "@request.auth.isApproved = true",
    "deleteRule": "@request.auth.isApproved = true",
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("3qttk2rdzkzv348");

  return dao.deleteCollection(collection);
})
