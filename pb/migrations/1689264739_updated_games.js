migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("78srpjtdesw2wqj")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "nwhggjb3",
    "name": "image",
    "type": "file",
    "required": true,
    "unique": false,
    "options": {
      "maxSelect": 1,
      "maxSize": 5242880,
      "mimeTypes": [
        "image/svg+xml",
        "image/png",
        "image/jpeg",
        "image/webp",
        "image/heic",
        "image/heif"
      ],
      "thumbs": [],
      "protected": false
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("78srpjtdesw2wqj")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "nwhggjb3",
    "name": "image",
    "type": "file",
    "required": true,
    "unique": false,
    "options": {
      "maxSelect": 1,
      "maxSize": 5242880,
      "mimeTypes": [
        "image/svg+xml",
        "image/png",
        "image/jpeg",
        "image/webp"
      ],
      "thumbs": [],
      "protected": false
    }
  }))

  return dao.saveCollection(collection)
})
