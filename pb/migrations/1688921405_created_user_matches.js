migrate((db) => {
  const collection = new Collection({
    "id": "e9iw5fjo0rt4ggh",
    "created": "2023-07-09 16:50:05.478Z",
    "updated": "2023-07-09 16:50:05.478Z",
    "name": "user_matches",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "k2h8fv2p",
        "name": "user",
        "type": "relation",
        "required": true,
        "unique": false,
        "options": {
          "collectionId": "_pb_users_auth_",
          "cascadeDelete": false,
          "minSelect": null,
          "maxSelect": 1,
          "displayFields": []
        }
      },
      {
        "system": false,
        "id": "z3s27skw",
        "name": "match",
        "type": "relation",
        "required": true,
        "unique": false,
        "options": {
          "collectionId": "lw5k1rn1dlo3vtx",
          "cascadeDelete": false,
          "minSelect": null,
          "maxSelect": 1,
          "displayFields": []
        }
      },
      {
        "system": false,
        "id": "uxonmxht",
        "name": "team",
        "type": "relation",
        "required": true,
        "unique": false,
        "options": {
          "collectionId": "6cm1kdkve4vs17l",
          "cascadeDelete": false,
          "minSelect": null,
          "maxSelect": 1,
          "displayFields": []
        }
      }
    ],
    "indexes": [],
    "listRule": "",
    "viewRule": "",
    "createRule": "@request.auth.isApproved = true",
    "updateRule": "@request.auth.isApproved = true",
    "deleteRule": "@request.auth.isApproved = true",
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("e9iw5fjo0rt4ggh");

  return dao.deleteCollection(collection);
})
