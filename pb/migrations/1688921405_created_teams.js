migrate((db) => {
  const collection = new Collection({
    "id": "6cm1kdkve4vs17l",
    "created": "2023-07-09 16:50:05.478Z",
    "updated": "2023-07-09 16:50:05.478Z",
    "name": "teams",
    "type": "base",
    "system": false,
    "schema": [
      {
        "system": false,
        "id": "qnr5w0ml",
        "name": "match",
        "type": "relation",
        "required": true,
        "unique": false,
        "options": {
          "collectionId": "lw5k1rn1dlo3vtx",
          "cascadeDelete": false,
          "minSelect": null,
          "maxSelect": 1,
          "displayFields": []
        }
      },
      {
        "system": false,
        "id": "8dgaiuru",
        "name": "name",
        "type": "text",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null,
          "pattern": ""
        }
      },
      {
        "system": false,
        "id": "d6y65yex",
        "name": "placement",
        "type": "number",
        "required": false,
        "unique": false,
        "options": {
          "min": null,
          "max": null
        }
      }
    ],
    "indexes": [],
    "listRule": "",
    "viewRule": "",
    "createRule": "@request.auth.isApproved = true",
    "updateRule": "@request.auth.isApproved = true",
    "deleteRule": "@request.auth.isApproved = true",
    "options": {}
  });

  return Dao(db).saveCollection(collection);
}, (db) => {
  const dao = new Dao(db);
  const collection = dao.findCollectionByNameOrId("6cm1kdkve4vs17l");

  return dao.deleteCollection(collection);
})
