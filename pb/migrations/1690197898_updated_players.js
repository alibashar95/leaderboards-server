migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("bmpv4hkmnnq9dvd")

  // add
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "sbmcu61c",
    "name": "unregistered",
    "type": "bool",
    "required": false,
    "unique": false,
    "options": {}
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("bmpv4hkmnnq9dvd")

  // remove
  collection.schema.removeField("sbmcu61c")

  return dao.saveCollection(collection)
})
