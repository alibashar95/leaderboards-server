migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("_pb_users_auth_")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "zjnoppir",
    "name": "player",
    "type": "relation",
    "required": true,
    "unique": false,
    "options": {
      "collectionId": "bmpv4hkmnnq9dvd",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("_pb_users_auth_")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "zjnoppir",
    "name": "player",
    "type": "relation",
    "required": false,
    "unique": false,
    "options": {
      "collectionId": "bmpv4hkmnnq9dvd",
      "cascadeDelete": false,
      "minSelect": null,
      "maxSelect": 1,
      "displayFields": []
    }
  }))

  return dao.saveCollection(collection)
})
