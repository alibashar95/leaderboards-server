migrate((db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("3qttk2rdzkzv348")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "4azwgzlr",
    "name": "third_place",
    "type": "number",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null
    }
  }))

  return dao.saveCollection(collection)
}, (db) => {
  const dao = new Dao(db)
  const collection = dao.findCollectionByNameOrId("3qttk2rdzkzv348")

  // update
  collection.schema.addField(new SchemaField({
    "system": false,
    "id": "4azwgzlr",
    "name": "thrid_places",
    "type": "number",
    "required": false,
    "unique": false,
    "options": {
      "min": null,
      "max": null
    }
  }))

  return dao.saveCollection(collection)
})
